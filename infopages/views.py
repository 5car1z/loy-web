from django.shortcuts import render
from members.models import Member

# Create your views here.
def index(request):
    return render(request, 'infopages/index.html')

def aboutus(request):
    founder_list = Member.objects.filter(founder_member=True)
    return render(request, 'infopages/aboutus.html', {'founders': founder_list})

def loyinfo(request):
    return render(request, 'infopages/loyinfo.html')
