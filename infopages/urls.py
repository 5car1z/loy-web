from django.conf.urls import include, url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^about/$', views.aboutus, name='aboutus'),
    url(r'^info/$', views.loyinfo, name='loyinfo'),
]
