$(document).ready(function() {
	$(".menu-icon").click(function() {
		$(this).toggleClass("menu-animation");
		var toggleWidth = $(".sidebar-wrapper").width() == 0 ? "250px" : "0px";
		var togglePadding = parseInt($("#wrapper").css('padding-left'),10) > 200;
		$(".sidebar-wrapper").animate({ width: toggleWidth }, { queue: false });
		$("#wrapper").animate({ 'padding-left': (togglePadding ? 0 : 250) });
	});
	$(".login-btn").click(function() {
		$(".pop").fadeIn(300);
	});
	$(".pop-header > span, .pop-header").click(function() {
		$(".pop").fadeOut(300);
	});
	$(".datepicker").datepicker({
		dateFormat: "d MM, yy",
		showAnim: "slideDown",
		yearRange: "-80:+0",
		changeMonth: true,
		changeYear: true
	});
});