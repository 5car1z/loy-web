# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0006_auto_20151006_1332'),
    ]

    operations = [
        migrations.AddField(
            model_name='member',
            name='profile_private',
            field=models.BooleanField(default=False),
        ),
    ]
