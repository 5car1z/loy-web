# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0003_auto_20151002_1036'),
    ]

    operations = [
        migrations.AddField(
            model_name='member',
            name='memorable_word',
            field=models.CharField(default='Word', max_length=20),
            preserve_default=False,
        ),
    ]
