# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0002_auto_20151002_1031'),
    ]

    operations = [
        migrations.AlterField(
            model_name='member',
            name='last_login',
            field=models.DateTimeField(auto_now_add=True, verbose_name=b'Last Logged in'),
        ),
    ]
