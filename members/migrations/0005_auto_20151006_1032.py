# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0004_member_memorable_word'),
    ]

    operations = [
        migrations.AddField(
            model_name='member',
            name='confirm_id',
            field=models.CharField(default='Test', max_length=128),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='member',
            name='email_confirm',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='member',
            name='active_member',
            field=models.BooleanField(default=False),
        ),
    ]
