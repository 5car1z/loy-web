# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0005_auto_20151006_1032'),
    ]

    operations = [
        migrations.AlterField(
            model_name='member',
            name='confirm_id',
            field=models.CharField(max_length=128, null=True),
        ),
    ]
