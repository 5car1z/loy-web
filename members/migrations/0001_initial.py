# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Member',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user_name', models.CharField(unique=True, max_length=30)),
                ('user_name_mask', models.CharField(max_length=30, verbose_name=b'User Name')),
                ('password', models.CharField(max_length=255)),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=75)),
                ('gender', models.CharField(default=b'NA', max_length=2, choices=[(b'MA', b'Male'), (b'FM', b'Female'), (b'NA', b'None')])),
                ('dob', models.DateField(verbose_name=b'Date of Birth')),
                ('email', models.EmailField(max_length=254)),
                ('join_date', models.DateTimeField(auto_now_add=True, verbose_name=b'Joined Date')),
                ('last_login', models.DateTimeField(verbose_name=b'Last Logged in')),
                ('active_member', models.BooleanField(default=True)),
                ('admin', models.BooleanField(default=False)),
                ('founder_member', models.BooleanField(default=False)),
            ],
        ),
    ]
