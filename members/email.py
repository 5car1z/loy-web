from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
from .models import Member
import string
import random

class EmailUser():
    plaintext = get_template('members/emailactivate.txt')
    htmly = get_template('members/emailactivate.html')
    from_email = 'leagueofyorkshire@gmail.com'
    
    def __init__(self, email, username):
        self.email_id = self.id_generator()
        self.to_mail = email
        self.user_link = 'www.leagueofyorkshire.co.uk/member/%s/%s' % (username.lower(), self.email_id)
        self.con_user = Context({'username': username, 'account_link': self.user_link})

    def send_email(self, subject='Activate Account'):
        text_content = self.plaintext.render(self.con_user)
        html_content = self.htmly.render(self.con_user)
        msg = EmailMultiAlternatives(subject, text_content, self.from_email, [self.to_mail])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        return self.email_id
        
    def id_generator(self, size=128, chars=string.ascii_uppercase + string.digits):
        return ''.join(random.choice(chars) for _ in range(size))

