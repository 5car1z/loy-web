from django.conf.urls import include, url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),

    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^register/$', views.register, name='register'),
    url(r'^forgot/$', views.forgotpassword, name='forgotpassword'),
    url(r'^(?P<username>\w+)/$', views.profileview, name='profile'),
    url(r'^(?P<username>\w+)/private/$', views.privateprofile, name='private'),
    url(r'^(?P<username>\w+)/(?P<emailid>\w+)/$', views.emailconfirm, name='emailconfirm'),
]
