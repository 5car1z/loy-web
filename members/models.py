from django.db import models
from datetime import date
from django.contrib.auth.hashers import make_password

class Member(models.Model):
    GENDER = (
    ('MA', 'Male'),
    ('FM', 'Female'),
    ('NA', 'None'),
        )
    
    user_name = models.CharField(max_length=30, unique=True)
    user_name_mask = models.CharField('User Name', max_length=30)
    password = models.CharField(max_length=255)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=75)
    gender = models.CharField(max_length=2,
                              choices=GENDER,
                              default='NA')
    dob = models.DateField('Date of Birth')
    email = models.EmailField(unique=True)
    join_date = models.DateTimeField('Joined Date', auto_now_add=True)
    last_login = models.DateTimeField('Last Logged in', auto_now_add=True)
    active_member= models.BooleanField(default=False)
    admin = models.BooleanField(default=False)
    founder_member = models.BooleanField(default=False)

    email_confirm = models.BooleanField(default=False)
    confirm_id = models.CharField(max_length=128, null=True)
    memorable_word = models.CharField(max_length=20)
    profile_private = models.BooleanField('Private profile', default=False)
    def __str__(self):
        return self.user_name_mask
    
    def returnage(self):
        today = date.today()
        return today.year - self.dob.year - ((today.month, today.day) < (self.dob.month, self.dob.day))

    def reset_password(self, new_password):
        self.password = make_password(new_password)
        self.save()

    returnage.short_description = "Age"
