from django.shortcuts import render, get_object_or_404, redirect
from django.http import Http404, HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.hashers import make_password, check_password
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from loy.access import Access
from .email import EmailUser
from .models import Member
from datetime import datetime
from .forms import LoginForm, RegisterForm, ForgotForm, SearchForm
import logging

logger = logging.getLogger(__name__)

def index(request):
    if request.method == 'POST':
        form = SearchForm(request.POST)
        member_list = Member.objects.filter(active_member=True)
        if form.is_valid():
            s_value = form.cleaned_data['search_field']
            member_list = member_list.filter(user_name__contains=s_value)
    else:
        form = SearchForm()
        member_list = Member.objects.filter(active_member=True)
    member_list = member_list.order_by('-join_date', 'last_login')
    paginator = Paginator(member_list,20)
    page = request.GET.get('page')
    try:
        members = paginator.page(page)
    except PageNotAnInteger:
        members = paginator.page(1)
    except EmptyPage:
        members = paginator.page(paginator.num_pages)
    return render(request, 'members/index.html', {'members': members, 'form': form})

def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            user = form.cleaned_data['user_name']
            m = Member.objects.get(user_name=user.lower())
            m.last_login = datetime.now()
            try:
                m.save()
                request.session['member_id'] = m.id
            except Exception, e:
                return render(request,'members/login_error.html', {'form': form, 'error_message': e})
            else:
                return HttpResponseRedirect(request.META.get('HTTP_REFERER','/'))
    else:
        form = LoginForm()
    return render(request,'members/login_error.html', {'form': form})

def logout(request):
    del request.session['member_id']
    return HttpResponseRedirect(request.META.get('HTTP_REFERER','/'))

def forgotpassword(request):
    if request.method == 'POST':
        form = ForgotForm(request.POST)
        if form.is_valid():
            try:
                m = Member.objects.get(user_name=form.cleaned_data['user_name'].lower())
                m.reset_password(form.cleaned_data['new_password'])
            except Exception, e:
                return render(request, 'members/register.html', {
                    'form': form,
                    'error_message': e})
            else:
                return redirect('/')
    else:
        form = ForgotForm()
    return render(request, 'members/forgot_password.html', {'form': form})

def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            try:
                e = EmailUser(form.cleaned_data['email'], form.cleaned_data['user_name'])
                m = Member(user_name=form.cleaned_data['user_name'].lower(),
                           user_name_mask=form.cleaned_data['user_name'],
                           password=make_password(form.cleaned_data['password']),
                           first_name=form.cleaned_data['first_name'],
                           last_name=form.cleaned_data['last_name'],
                           gender=form.cleaned_data['gender'],
                           dob=form.cleaned_data['dob'],
                           email=form.cleaned_data['email'],
                           memorable_word=form.cleaned_data['memorable_word'],
                           confirm_id=e.send_email())
                m.save()
            except Exception, e:
                return render(request, 'members/register.html', {
                    'form': form,
                    'error_message': e})
            else:
                #redirect to success page to be set up
                return redirect('/')
    else:
        form = RegisterForm()
    return render(request, 'members/register.html', {'form': form})

def emailconfirm(request, username, emailid):
    m = get_object_or_404(Member, user_name=username)
    print m.confirm_id
    if not m.confirm_id or (m.confirm_id != emailid):
        raise Http404()
    m.email_confirm = True
    m.active_member = True
    m.confirm_id = None
    m.save()
    return render(request, 'members/accountactivated.html', {'username': m.user_name_mask})

def profileview(request, username):
    m = get_object_or_404(Member, user_name=username)
    return render(request, 'members/profileview.html', {'member': m})

def privateprofile(request, username):
    m = get_object_or_404(Member, user_name=username)
    try:
        r_id = request.session['member_id']
    except KeyError:
        raise Http404()
    if r_id != m.id:
        raise Http404()
    if m.profile_private:
        m.profile_private = False
    else:
        m.profile_private = True
    m.save()
    return HttpResponseRedirect('/member/%s' % username)

