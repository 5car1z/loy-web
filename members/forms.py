from django import forms
from django.contrib.auth.hashers import check_password
import re
from .models import Member

class LoginForm(forms.Form):
    user_name = forms.CharField(label='User Name', max_length=30, widget=forms.TextInput(attrs={
        'class': 'form-control'}))
    password = forms.CharField(label='Password', max_length=255, widget=forms.PasswordInput(attrs={
        'class': 'form-control'}))
    def clean(self):
        cleaned_data = super(LoginForm, self).clean()
        try:
            user_name = self.cleaned_data['user_name']
            password = self.cleaned_data['password']
        except:
            return
        else:
            if not self.username_check(user_name, password):
                self.add_error('user_name', 'Invalid User Name/Password or Account not active')
            
    def username_check(self, username, password):
        try:
            m = Member.objects.get(user_name=str(username).lower())
        except:
            return False
        else:
            if not check_password(password, m.password) or not m.active_member:
                return False
            return True

class RegisterForm(forms.Form):
    user_name = forms.CharField(label="User Name", max_length=30, widget=forms.TextInput(attrs={
        'class': 'form-control', 'placeholder': 'User Name'}))
    first_name = forms.CharField(label="First Name", max_length=50, widget=forms.TextInput(attrs={
        'class': 'form-control', 'placeholder': 'First Name'}))
    last_name = forms.CharField(label="Last Name", max_length=75, widget=forms.TextInput(attrs={
        'class': 'form-control', 'placeholder': 'Last Name'}))
    dob = forms.DateField(label="Date of Birth", widget=forms.DateInput(attrs={
        'class': 'form-control datepicker', 'placeholder': 'Date of Birth'}), input_formats=
                          ['%d %B, %Y', '%d %b, %Y', '%d %b, %y', '%d/%m/%Y', '%d/%m/%y'])
    gender = forms.ChoiceField(label="Gender", choices=Member.GENDER, widget=forms.Select(attrs={
        'class': 'form-control'}))
    email = forms.EmailField(label="Email", max_length=254, widget=forms.TextInput(attrs={
        'class': 'form-control', 'placeholder': 'Email Address'}))
    memorable_word = forms.CharField(label="Memborable Word", max_length=20, widget=forms.TextInput(attrs={
        'class': 'form-control', 'placeholder': 'Used for password resets'}))
    password = forms.CharField(label='Password', max_length=255, widget=forms.PasswordInput(attrs={
        'class': 'form-control', 'placeholder': 'Password'}))
    password_confirm = forms.CharField(label='Confirm Password', max_length=255, widget=forms.PasswordInput(attrs={
        'class': 'form-control', 'placeholder': 'Confirm Password'}))

    def clean(self):
        cleaned_data = super(RegisterForm, self).clean()
        user_name = self.cleaned_data.get('user_name', '')
        email = self.cleaned_data.get('email', '')
        password = self.cleaned_data.get('password', None)
        pass_confirm = self.cleaned_data.get('password_confirm', None)
        if not re.match("^[a-zA-Z0-9_]*$",user_name) or len(user_name) < 3:
            self.add_error('user_name', 'User Name contains an invalid charater or it is too short')
        if not self.unique_check(user_name):
            self.add_error('user_name', 'User Name is already being used')
        if not self.unique_check(email, True):
            self.add_error('email', 'Email address is already being used')
        if not passwordCheck(password, pass_confirm):
            self.add_error('password', "Password must be longer than 8 charaters")
            self.add_error('password_confirm', "Passwords need to match")

    def unique_check(self, value, email=False):
        try:
            if email:
                m = Member.objects.get(email=value)
            else:
                m = Member.objects.get(user_name=str(value).lower())
        except:
            return True
        else:
            return False

class ForgotForm(forms.Form):
    user_name = forms.CharField(label="User Name", max_length=30, widget=forms.TextInput(attrs={
        'class': 'form-control', 'placeholder': 'User Name'}))
    email = forms.EmailField(label="Email", max_length=254, widget=forms.TextInput(attrs={
        'class': 'form-control', 'placeholder': 'Email Address'}))
    memorable_word = forms.CharField(label="Memborable Word", max_length=20, widget=forms.TextInput(attrs={
        'class': 'form-control', 'placeholder': 'Enter memorable word'}))
    new_password = forms.CharField(label='New Password', max_length=255, widget=forms.PasswordInput(attrs={
        'class': 'form-control', 'placeholder': 'Password'}))
    password_confirm = forms.CharField(label='Confirm Password', max_length=255, widget=forms.PasswordInput(attrs={
        'class': 'form-control', 'placeholder': 'Confirm Password'}))

    def clean(self):
        cleaned_data = super(ForgotForm, self).clean()
        username = self.cleaned_data.get('user_name', None)
        email = self.cleaned_data.get('email', None)
        mem_word = self.cleaned_data.get('memorable_word', None)
        password = self.cleaned_data.get('new_password', None)
        pass_confirm = self.cleaned_data.get('password_confirm', None)
        if username and email:
            try:
                m = Member.objects.get(user_name=username.lower(), email=email)
            except:
                self.add_error('user_name', "No match found")
            else:
                if m.memorable_word != mem_word:
                    self.add_error('memorable_word', "Doesn't match")
        if not passwordCheck(password, pass_confirm):
            self.add_error('new_password', "Password must be longer than 8 charaters")
            self.add_error('password_confirm', "Passwords need to match")

class SearchForm(forms.Form):
    search_field = forms.CharField(label="", max_length=50, required=False, widget=forms.TextInput(attrs={
        'class': 'form-control', 'placeholder': 'Search'}))

def passwordCheck(password, confirm_password):
    if not password or not confirm_password:
        return False
    if password and confirm_password and (password != confirm_password):
        return False
    if len(password) < 8:
        return False
    return True
        
    
