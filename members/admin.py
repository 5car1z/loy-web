from django.contrib import admin
from .models import Member

class MemberAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields':['user_name_mask', 'admin', 'founder_member', 'active_member']}),
        ('Personal Information', {'fields': ['first_name', 'last_name', 'dob', 'gender', 'profile_private']}),
        ('Email Information', {'fields': ['email', 'email_confirm', 'confirm_id']}),
    ]
    list_display = ('user_name_mask', 'first_name', 'last_name', 'gender', 'returnage', 'admin', 'active_member')
    search_fields = ['user_name_mask', 'first_name', 'last_name']

admin.site.register(Member, MemberAdmin)
