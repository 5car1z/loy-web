from django import template
from django.template.defaultfilters import stringfilter
from members.models import Member


register = template.Library()

@register.filter
@stringfilter
def return_username(value, mask=True):
    try:
        m = Member.objects.get(pk=value)
    except:
        return 'ERROR No Username found'
    else:
        if mask:
            return m.user_name_mask
        else:
            return m.user_name

@register.filter
def return_gender(value):
    return dict(Member.GENDER)[value]
