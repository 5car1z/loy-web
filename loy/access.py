from django.shortcuts import render


class Access():
    def __init__(self, request):
        self.request = request
        self.sid = self.request.session.get('member_id')

    def check(self):
        if not self.sid:
            return False
        else:
            return True

    def noaccess(self):
        return render(self.request, 'no_access.html')
        
