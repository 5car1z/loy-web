#[user/machine specific settings]

# Make a copy of this file and name the new file credentials.py then fill in the various values in the new file. 

# Never track or commit the new "credentials.py" file, it should be set as ignored in your .gitignore.

SECRET_KEY = ''

DEBUG = 

TEMPLATE_DEBUG = 

# "Live" Relevant Settings 
# import os
# ALLOWED_HOSTS = ['', '', '']
# BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# STATIC_URL = '/static/'
# STATIC_ROOT = os.path.join(BASE_DIR, "assets/")
# STATICFILES_DIRS = (os.path.join('static'),)

#DATABASE_ENGINE = 'mysql'
#DATABASE_NAME = 'mydbname'
#DATABASE_USER = 'mydbuser'
#DATABASE_PASSWORD = 'mydbpassword'
#DATABASE_HOST = 'localhost'
#DATABASE_PORT = ''

EMAIL_HOST = ''
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_PORT = '' 
EMAIL_USE_TLS = 
