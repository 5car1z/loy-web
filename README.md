# League of Yorkshire Website

![LoY Logo](http://i.imgur.com/RmdBEyj.png)

http://www.leagueofyorkshire.co.uk

## Cloning Repository

```bash
$ git clone https://github.com/5car1z/loy-web.git
```

>There are no submodules to initialise.

## Credentials Template File Instructions 
Make a copy of the `credentials_template.py` file. Then rename the new file `credentials.py` and fill in the various values to the latter. 

```bash
$ cd loy/
$ cp credentials_template.py credentials.py
$ $EDITOR credentials.py 
```

Never track or commit the new `credentials.py` file, it should be set as ignored in your `.gitignore`. Of which there is a repository version already created and set.
